# Installation

- Clone custom message pkg 'ugv_interfaces' to '/mcu_ws' folder
```
cd ~/microros_ws/firmware/mcu_ws
git clone code here
```
- Clone tof_reader fake node to '/apps' folder
```
cd ~/microros_ws/firmware/apps
git clone code here
```

- Configure, build and flash firmware

```
cd ~/microros_ws
source install/local_setup.bash

ros2 run micro_ros_setup configure_firmware.sh tof_reader_fake_node -t serial
ros2 run micro_ros_setup build_firmware.sh
ros2 run micro_ros_setup flash_firmware.sh
```

- Create agent if not done already
```
ros2 run micro_ros_setup create_agent_ws.sh
ros2 run micro_ros_setup build_agent.sh
source install/local_setup.bash
```
- Otherwise, start agent (may have to replug usb. fix coming soon)
```
ros2 run micro_ros_agent micro_ros_agent serial --dev /dev/ttyUSB0
```

- Check topics using another cmd window
```
ros2 topic list
```



